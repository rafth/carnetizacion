<x-app-layout>
    <x-slot name="header_content">
        <h1>Carnets por imprimir</h1>
        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item active"><a href="#">Inicio</a></div>
            <div class="breadcrumb-item">Carnets por imprimir</div>
        </div>
    </x-slot>

    <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">
        <livewire:carnets.carnets-por-imprimir-component /> 
    </div>
</x-app-layout>