<div>
    <div class="card-header">
        <h4>RESUMEN DE SOLICITUDES RECIBIDAS </h4>
        <div class="card-header-form col-4">
            <input type="text" class="form-control mt-0.5" placeholder="Buscar por N° de Solicitud o por cédula..."
                wire:model.lazy="search" onkeyup="mayus(this);">
        </div>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-striped" id="table-1">
                <thead>
                    <tr>
                        <th class="text-center">
                            N° de Solicitud
                        </th>
                        <th class="text-center">Cédula</th>
                        <th class="text-center">Nombre</th>
                        <th class="text-center">Cargo</th>
                        <th class="text-center">Unidad laboral</th>
                        <th class="text-center">Fecha de registro</th>
                        <th class="text-center">Estado</th>
                        <th class="text-center">Acción</th>
                    </tr>
                </thead>
                <tbody>

                    <tr>
                        <td class="text-center">
                            28.143.033
                        </td>

                        <td class="text-center">
                            ROXLENE VERA
                        </td>

                        <td class="text-center">
                            CARGO
                        </td>

                        <td class="text-center">
                            UNIDAD LABORAL
                        </td>

                        <td class="text-center">
                            FECJ DE REGISTRO
                        </td>

                        <td class="text-center">
                            ESTADOS
                        </td>

                        <td class="text-center">
                            ACCIÓN
                        </td>
                        <td class="text-center">
                            ACCIÓN
                        </td>
                    </tr>
                    <tr>
                        <td class="text-center">
                            28.143.033
                        </td>

                        <td class="text-center">
                            ROXLENE VERA
                        </td>

                        <td class="text-center">
                            CARGO
                        </td>

                        <td class="text-center">
                            UNIDAD LABORAL
                        </td>

                        <td class="text-center">
                            FECJ DE REGISTRO
                        </td>

                        <td class="text-center">
                            ESTADOS
                        </td>

                        <td class="text-center">
                            ACCIÓN
                        </td>
                        <td class="text-center">
                            ACCIÓN
                        </td>
                    </tr>
                </tbody>
            </table>
            <div class="row col-12 mb-4">
                <div class="col-md-6 form-inline text-left">
                    Por página: &nbsp;
                    <select wire:model="perPage" class="form-control">
                        <option>5</option>
                        <option>10</option>
                        <option>15</option>
                        <option>25</option>
                    </select>
                </div>
                <div class="col-md-6 text-right">

                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
</div>