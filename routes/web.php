<?php

use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group([ "middleware" => ['auth:sanctum', config('jetstream.auth_session'), 'verified'] ], function() {
    Route::view('/dashboard', "dashboard")->name('dashboard');

    Route::get('/user', [ UserController::class, "index_view" ])->name('user');
    Route::view('/user/new', "pages.user.user-new")->name('user.new');
    Route::view('/user/edit/{userId}', "pages.user.user-edit")->name('user.edit');

    Route::view('/carnets/carnets-por-imprimir', "carnets.carnets-por-imprimir")->name('carnets.carnets-por-imrpimir');
    Route::view('/carnets/carnets-impresos', "carnets.carnets-impresos")->name('carnets.carnets-impresos');
    Route::view('/carnets/carnets-desactivados', "carnets.carnets-desactivados")->name('carnets.carnets-desactivados');
    Route::view('/carnets/carnets-manual', "carnets.carnets-manual")->name('carnets.carnets-manual');
});
