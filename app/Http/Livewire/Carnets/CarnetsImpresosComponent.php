<?php

namespace App\Http\Livewire\Carnets;

use Livewire\Component;

class CarnetsImpresosComponent extends Component
{
    public function render()
    {
        return view('livewire..carnets.carnets-impresos-component');
    }
}
