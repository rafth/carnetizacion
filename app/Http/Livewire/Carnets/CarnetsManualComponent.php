<?php

namespace App\Http\Livewire\Carnets;

use Livewire\Component;

class CarnetsManualComponent extends Component
{
    public function render()
    {
        return view('livewire..carnets.carnets-manual-component');
    }
}
