<?php

namespace App\Http\Livewire\Carnets;

use Livewire\Component;

class CarnetsDesactivosComponent extends Component
{
    public function render()
    {
        return view('livewire..carnets.carnets-desactivos-component');
    }
}
