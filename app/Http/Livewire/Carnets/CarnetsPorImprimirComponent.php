<?php

namespace App\Http\Livewire\Carnets;

use Livewire\Component;

class CarnetsPorImprimirComponent extends Component
{
    public function render()
    {
        return view('livewire..carnets.carnets-por-imprimir-component');
    }
}
